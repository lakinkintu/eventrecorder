package com.example.dto;

/**
 * Created by C-13 on 6/1/2015.
 */
public class EventDTO {
    private String eventName;
    private boolean isPosition;

    public boolean isPosition() {
        return isPosition;
    }

    public void setPosition(boolean isPosition) {
        this.isPosition = isPosition;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }
}
