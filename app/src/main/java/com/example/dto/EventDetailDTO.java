package com.example.dto;

/**
 * Created by C-13 on 6/2/2015.
 */
public class EventDetailDTO {
    private int id;
    private int eventId;
    private String eventName;
    private String date;
    private String time;
    private String latitude;
    private String longitude;
    private String situation;
    private String text;
    private String numeric;


    public EventDetailDTO(int id, int eventId, String eventName, String date, String time, String latitude, String longitude) {
        this.id = id;
        this.eventId = eventId;
        this.eventName = eventName;
        this.date = date;
        this.time = time;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public EventDetailDTO(int id, int eventId, String eventName, String date, String time, String latitude, String longitude, String situation, String text, String numeric) {
        this.id = id;
        this.eventId = eventId;
        this.eventName = eventName;
        this.date = date;
        this.time = time;
        this.latitude = latitude;
        this.longitude = longitude;
        this.situation = situation;
        this.text = text;
        this.numeric = numeric;
    }

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getNumeric() {
        return numeric;
    }

    public void setNumeric(String numeric) {
        this.numeric = numeric;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

}
