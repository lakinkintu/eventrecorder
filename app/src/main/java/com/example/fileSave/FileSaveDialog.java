//package com.example.fileSave;
//
//
//        import android.os.Bundle;
//        import android.app.Activity;
//        import android.view.Menu;
//        import android.view.View;
//
//        import android.widget.Button;
//        import android.view.View.OnClickListener;
//        import android.widget.Toast;
//
////import android.view.View;
//
//public class MainActivity extends Activity {
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//
//        //Button1
//        Button dirChooserButton1 = (Button) findViewById(R.id.button1);
//        dirChooserButton1.setOnClickListener(new OnClickListener()
//        {
//            String m_chosen;
//            @Override
//            public void onClick(View v) {
//                /////////////////////////////////////////////////////////////////////////////////////////////////
//                //Create FileOpenDialog and register a callback
//                /////////////////////////////////////////////////////////////////////////////////////////////////
//                SimpleFileDialog FileOpenDialog =  new SimpleFileDialog(MainActivity.this, "FileOpen",
//                        new SimpleFileDialog.SimpleFileDialogListener()
//                        {
//                            @Override
//                            public void onChosenDir(String chosenDir)
//                            {
//                                // The code in this function will be executed when the dialog OK button is pushed
//                                m_chosen = chosenDir;
//                                Toast.makeText(MainActivity.this, "Chosen FileOpenDialog File: " +
//                                        m_chosen, Toast.LENGTH_LONG).show();
//                            }
//                        });
//
//                //You can change the default filename using the public variable "Default_File_Name"
//                FileOpenDialog.Default_File_Name = "";
//                FileOpenDialog.chooseFile_or_Dir();
//
//                /////////////////////////////////////////////////////////////////////////////////////////////////
//
//            }
//        });
//
//        //Button2
//        Button dirChooserButton2 = (Button) findViewById(R.id.button2);
//        dirChooserButton2.setOnClickListener(new OnClickListener()
//        {
//            String m_chosen;
//            @Override
//            public void onClick(View v) {
//                /////////////////////////////////////////////////////////////////////////////////////////////////
//                //Create FileSaveDialog and register a callback
//                /////////////////////////////////////////////////////////////////////////////////////////////////
//                SimpleFileDialog FileSaveDialog =  new SimpleFileDialog(MainActivity.this, "FileSave",
//                        new SimpleFileDialog.SimpleFileDialogListener()
//                        {
//                            @Override
//                            public void onChosenDir(String chosenDir)
//                            {
//                                // The code in this function will be executed when the dialog OK button is pushed
//                                m_chosen = chosenDir;
//                                Toast.makeText(MainActivity.this, "Chosen FileOpenDialog File: " +
//                                        m_chosen, Toast.LENGTH_LONG).show();
//                            }
//                        });
//
//                //You can change the default filename using the public variable "Default_File_Name"
//                FileSaveDialog.Default_File_Name = "my_default.txt";
//                FileSaveDialog.chooseFile_or_Dir();
//
//                /////////////////////////////////////////////////////////////////////////////////////////////////
//
//            }
//        });
//
//        //Button3
//        Button dirChooserButton3 = (Button) findViewById(R.id.button3);
//        dirChooserButton3.setOnClickListener(new OnClickListener()
//        {
//            String m_chosen;
//            @Override
//            public void onClick(View v) {
//
//                /////////////////////////////////////////////////////////////////////////////////////////////////
//                //Create FileOpenDialog and register a callback
//                /////////////////////////////////////////////////////////////////////////////////////////////////
//                SimpleFileDialog FolderChooseDialog =  new SimpleFileDialog(MainActivity.this, "FolderChoose",
//                        new SimpleFileDialog.SimpleFileDialogListener()
//                        {
//                            @Override
//                            public void onChosenDir(String chosenDir)
//                            {
//                                // The code in this function will be executed when the dialog OK button is pushed
//                                m_chosen = chosenDir;
//                                Toast.makeText(MainActivity.this, "Chosen FileOpenDialog File: " +
//                                        m_chosen, Toast.LENGTH_LONG).show();
//                            }
//                        });
//
//                FolderChooseDialog.chooseFile_or_Dir();
//
//                /////////////////////////////////////////////////////////////////////////////////////////////////
//
//            }
//        });
//
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//}
//
