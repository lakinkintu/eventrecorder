package com.example.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.dto.EventDetailDTO;
import com.example.eventrecorder.EventDataList;
import com.example.eventrecorder.EventList;
import com.example.eventrecorder.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class TestAdapter {
	protected static final String TAG = "DataAdapter";

	private final Context mContext;
	private SQLiteDatabase mDb;
	private DataBaseHelper mDbHelper;


	public TestAdapter(Context context) {
		this.mContext = context;
		mDbHelper = new DataBaseHelper(mContext);
	}

	public TestAdapter createDatabase() throws SQLException {
		try {
			mDbHelper.createDataBase();
		} catch (IOException mIOException) {
			Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
			throw new Error("UnableToCreateDatabase");
		}
		return this;
	}

	public TestAdapter open() throws SQLException {
		try {
			mDbHelper.openDataBase();
			mDbHelper.close();
			mDb = mDbHelper.getReadableDatabase();
		} catch (SQLException mSQLException) {
			Log.e(TAG, "open >>" + mSQLException.toString());
			throw mSQLException;
		}
		return this;
	}

	public void close() {
		mDbHelper.close();
	}

	public Cursor selectQuery(String query) {
		Cursor c1 = null;
		try {

			if (mDb.isOpen()) {
				mDb.close();
			}
			mDb = mDbHelper.getWritableDatabase();
			c1 = mDb.rawQuery(query, null);

		} catch (Exception e) {

			System.out.println("DATABASE ERROR 10 " + e);

		}
		return c1;

	}

	public void executeQuery(String query) {
		try {

			if (mDb.isOpen()) {
				mDb.close();
			}

			mDb = mDbHelper.getWritableDatabase();
			mDb.execSQL(query);

		} catch (Exception e) {

			System.out.println("DATABASE ERROR 1 " + e);
		}
	}

	public void deleteQuery(String query) {
		try {
			if (mDb.isOpen()) {
				mDb.close();
			}
			mDb = mDbHelper.getWritableDatabase();
			mDb.execSQL(query);
		} catch (Exception e) {
			System.out.println("DATABASE ERROR  2" + e);
		}

	}

	public void updateQuery(String query) {
		try {

			if (mDb.isOpen()) {
				mDb.close();
			}

			mDb = mDbHelper.getWritableDatabase();
			mDb.execSQL(query);

		} catch (Exception e) {

			System.out.println("DATABASE ERROR 3 " + e);
		}

	}

	/**
	 * @param contentValues
	 * @return
	 * 
	 */
	public Long saveEvent(ContentValues contentValues) {
		try {
			Long id = mDb.insert("event_tbl", null, contentValues);

			Log.d("Save category", "informationsaved");
			return id;

		} catch (Exception ex) {
			Log.d("Save event", ex.toString());
			ex.printStackTrace();
			return -1L;
		}

	}

	public boolean selectAlarm() {
		Cursor c1 = null;
		try {

			if (mDb.isOpen()) {
				mDb.close();
			}
			mDb = mDbHelper.getWritableDatabase();
			c1 = mDb.rawQuery("select * from alarm_tbl", null);
			if (c1 != null && c1.getCount() > 0) {
				if (c1.moveToFirst()) {
					System.out.println("tasktable");
					do {
						System.out
								.println("c1.getString(c1.getColumnIndex(\"job_name\")) :: "
										+ c1.getString(c1
												.getColumnIndex("job_name")));
						System.out
								.println("c1.getColumnIndex(\"place_name\"):: "
										+ c1.getString(c1
												.getColumnIndex("place_name")));
					} while (c1.moveToNext());
				}
			}
			c1.close();// closing connection
		} catch (Exception e) {
			System.out.println("DATABASE ERROR 8 " + e);
		}

		return false;

	}

	/**
	 * 
	 */
//	public AlarmDTO getClosestTask() {
//		AlarmDTO alarmDTO;
//		ArrayList<AlarmDTO> alarmList = new ArrayList<AlarmDTO>();
//		Cursor c1 = null;
//		try {
//
//			if (mDb.isOpen()) {
//				mDb.close();
//			}
//			mDb = mDbHelper.getWritableDatabase();
//			c1 = mDb.rawQuery("select * from alarm_tbl where status != 'expire'", null);
//			if (c1 != null && c1.getCount() > 0) {
//				if (c1.moveToFirst()) {
//					do {
//						alarmDTO = new AlarmDTO();
//						alarmDTO.setId(c1.getString(c1.getColumnIndex("id")));
//						alarmDTO.setJobName(c1.getString(c1
//								.getColumnIndex("job_name")));
//						alarmDTO.setPlaceName(c1.getString(c1
//								.getColumnIndex("place_name")));
//						alarmDTO.setDay(Integer.parseInt(c1.getString(
//								c1.getColumnIndex("day")).trim()));
//						alarmDTO.setMonth(c1.getString(c1.getColumnIndex("month")));
//						alarmDTO.setMonthNum(Integer.parseInt(c1.getString(c1
//								.getColumnIndex("month_num"))));
//						alarmDTO.setYear(Integer.parseInt(c1.getString(
//								c1.getColumnIndex("year")).trim()));
//						alarmDTO.setHour(Integer.parseInt(c1.getString(
//								c1.getColumnIndex("hour")).trim()));
//						alarmDTO.setMinute(Integer.parseInt((c1.getString(c1
//								.getColumnIndex("minute")).trim())));
//						alarmDTO = createEndDate(alarmDTO);
//						System.out.println(alarmDTO.getEndDateString());
//						alarmList.add(alarmDTO);
//
//					} while (c1.moveToNext());
//				}
//			}
//			c1.close();// closing connection
//			if(!alarmList.isEmpty()) {
//				return getFirstClosestTask(alarmList);
//			}else {
//				return null;
//			}
//
//
//		} catch (Exception e) {
//			System.out.println("DATABASE ERROR 8 " + e);
//			e.printStackTrace();
//			return null;
//		}
//
//	}

//	/**
//	 * @param alarmDTO
//	 * @return
//	 *
//	 */
//	private AlarmDTO createEndDate(AlarmDTO alarmDTO) {
//
//		String string = "01/14/2012 09:29:58";
//		String endDate = "" + alarmDTO.getMonthNum() + "/" + alarmDTO.getDay()
//				+ "/" + alarmDTO.getYear() + " " + alarmDTO.getHour() + ":"
//				+ alarmDTO.getMinute() + ":" + "00";
//		alarmDTO.setEndDatestring(endDate);
//		return alarmDTO;
//	}
//
//	/**
//	 * @param alarmList
//	 * @return
//	 *
//	 */
//	private AlarmDTO getFirstClosestTask(ArrayList<AlarmDTO> alarmList) {
//		AlarmDTO alarmDTO = Collections.min(alarmList,new AlarmComp());
//
//		return alarmDTO;
//	}

	/**
	 * @param id
	 * @return
	 */
	public void updateTask(ContentValues cv, String id) {
		try {

			if (mDb.isOpen()) {
				mDb.close();
			}

			mDb = mDbHelper.getWritableDatabase();
			mDb.update("alarm_tbl", cv, "id = ?", new String[] { id });

		} catch (Exception e) {

			System.out.println("DATABASE ERROR 4 " + e);
		}
		// return 0;
	}

	/**
	 * @return 
	 * 
	 */
	public boolean isJobExist() {
		boolean exist = false;
		Cursor c1 = null;
		try {

			if (mDb.isOpen()) {
				mDb.close();
			}
			mDb = mDbHelper.getWritableDatabase();
			c1 = mDb.rawQuery("select * from alarm_tbl", null);
			if (c1 != null && c1.getCount() > 0) {
				exist = true;
				/*if (c1.moveToFirst()) {
					System.out.println("tasktable");
					do {
						System.out
								.println("c1.getString(c1.getColumnIndex(\"job_name\")) :: "
										+ c1.getString(c1
												.getColumnIndex("job_name")));
						System.out
								.println("c1.getColumnIndex(\"place_name\"):: "
										+ c1.getString(c1
												.getColumnIndex("place_name")));
					} while (c1.moveToNext());
				}*/
			} else {
				exist = false;
			}
			c1.close();// closing connection
		} catch (Exception e) {
			System.out.println("DATABASE ERROR 8 " + e);
			e.printStackTrace();
		}

		return exist;
		
	}

	/**
	 * @return
	 */
//	public ArrayList<AlarmDTO> getRunningTask() {
//		AlarmDTO alarmDTO;
//		ArrayList<AlarmDTO> alarmList = new ArrayList<AlarmDTO>();
//		Cursor c1 = null;
//		try {
//
//			if (mDb.isOpen()) {
//				mDb.close();
//			}
//			mDb = mDbHelper.getWritableDatabase();
//			c1 = mDb.rawQuery("select * from alarm_tbl where status = 'running'", null);
//			if (c1 != null && c1.getCount() > 0) {
//				if (c1.moveToFirst()) {
//					do {
//						alarmDTO = new AlarmDTO();
//						alarmDTO.setId(c1.getString(c1.getColumnIndex("id")));
//						alarmDTO.setJobName(c1.getString(c1
//								.getColumnIndex("job_name")));
//						alarmDTO.setPlaceName(c1.getString(c1
//								.getColumnIndex("place_name")));
//						alarmDTO.setDay(Integer.parseInt(c1.getString(
//								c1.getColumnIndex("day")).trim()));
//						alarmDTO.setMonth(c1.getString(c1.getColumnIndex("month")));
//						alarmDTO.setMonthNum(Integer.parseInt(c1.getString(c1
//								.getColumnIndex("month_num"))));
//						alarmDTO.setYear(Integer.parseInt(c1.getString(
//								c1.getColumnIndex("year")).trim()));
//						alarmDTO.setHour(Integer.parseInt(c1.getString(
//								c1.getColumnIndex("hour")).trim()));
//						alarmDTO.setMinute(Integer.parseInt((c1.getString(c1
//								.getColumnIndex("minute")).trim())));
//						alarmDTO.setMaxLeftMinute(Integer.parseInt((c1.getString(c1
//								.getColumnIndex("max_left_minute")).trim())));
//						alarmDTO.setMaxLeftSecond(Integer.parseInt((c1.getString(c1
//								.getColumnIndex("max_left_second")).trim())));
//						alarmDTO.setEndDatestring((c1.getString(c1
//								.getColumnIndex("end_date")).trim()));
//					//	alarmDTO = createEndDate(alarmDTO);
//						System.out.println(alarmDTO.getEndDateString());
//						alarmList.add(alarmDTO);
//
//
//					} while (c1.moveToNext());
//				}
//			}
//			c1.close();// closing connection
//
//
//
//		} catch (Exception e) {
//			System.out.println("DATABASE ERROR 8 " + e);
//			e.printStackTrace();
//			return null;
//		}
//		return alarmList;
//	}

	/**
	 * @return 
	 *
     * @param selectedEvent
     * @param seletedEventName
     */
	public ArrayList<EventDetailDTO> getEventDetailDTOList(int selectedEvent, String seletedEventName) {
        EventDetailDTO eventDetailDTO;
		ArrayList<EventDetailDTO> alarmList = new ArrayList<EventDetailDTO>();
		Cursor c1 = null;
        int id;
        int eventId;
        String date="";
        String time="";
        String latitude="";
        String longitude="";
        String eventName=seletedEventName;
        String situation="";
        String numeric="";
        String text="";
		try {

			if (mDb.isOpen()) {
				mDb.close();
			}
			mDb = mDbHelper.getWritableDatabase();


			c1 = mDb.rawQuery("select * from event_detail_tbl where event_id = '"+selectedEvent+"'", null);
			if (c1 != null && c1.getCount() > 0) {
				if (c1.moveToFirst()) {
					do {

                        id = c1.getInt(c1.getColumnIndex("id"));
                       eventId = c1.getInt(c1
								.getColumnIndex("event_id"));
                        date = c1.getString(c1
								.getColumnIndex("date"));
                        time = c1.getString(
								c1.getColumnIndex("time"));
                        latitude = c1.getString(c1.getColumnIndex("latitude"));
                        longitude = c1.getString(c1
								.getColumnIndex("longitude"));
                        situation = c1.getString(c1
                                .getColumnIndex("situation"));
                        numeric = c1.getString(c1
                                .getColumnIndex("numeric"));
                        text = c1.getString(c1
                                .getColumnIndex("text"));

                        eventDetailDTO = new EventDetailDTO(id,eventId,eventName,date,time,latitude,longitude,situation,text,numeric);
						alarmList.add(eventDetailDTO);


					} while (c1.moveToNext());
				}
			}
			c1.close();// closing connection



		} catch (Exception e) {
			System.out.println("DATABASE ERROR 8 " + e);
			e.printStackTrace();
			return null;
		}
		return alarmList;

	}


    public String getRecentData() {
        String eventName = "";
        Cursor c1 = null;
        try {

            c1 = mDb.rawQuery("SELECT * FROM event_tbl where id in( SELECT max(id) FROM event_tbl)", null);
			if (c1 != null && c1.getCount() > 0) {
				if (c1.moveToFirst()) {
//					do {
                        eventName = c1.getString(c1.getColumnIndex("event_name"));
//					} while (c1.moveToNext());
				}
			}
			c1.close();// closing connection


		} catch (Exception e) {
			System.out.println("DATABASE ERROR 9 " + e);
			e.printStackTrace();
			return eventName;
		}
		return eventName;


    }
//	public boolean deleteRecent() {
//
//		try {
//			int num = mDb.delete("event_tbl", null, new String[] { } );
//			Log.d("delete job", "information deleted");
//			if (num == 1) {
//				return true;
//			} else {
//				return false;
//			}
//
//		} catch (Exception ex) {
//			Log.d("Delete Job", ex.toString());
//			ex.printStackTrace();
//			return false;
//		}
//	}

//		public AlarmDTO getAlarmDTO(String id) {
//			AlarmDTO alarmDTO = null;
//			Cursor c1 = null;
//			try {
//
//				if (mDb.isOpen()) {
//					mDb.close();
//				}
//				mDb = mDbHelper.getWritableDatabase();
//				c1 = mDb.rawQuery("select * from alarm_tbl where id = "+id+"", null);
//				if (c1 != null && c1.getCount() > 0) {
//					if (c1.moveToFirst()) {
//						alarmDTO = new AlarmDTO();
//						alarmDTO.setId(c1.getString(c1.getColumnIndex("id")));
//						alarmDTO.setJobName(c1.getString(c1
//								.getColumnIndex("job_name")));
//						alarmDTO.setPlaceName(c1.getString(c1
//								.getColumnIndex("place_name")));
//						alarmDTO.setDay(Integer.parseInt(c1.getString(
//								c1.getColumnIndex("day")).trim()));
//						alarmDTO.setMonth(c1.getString(c1.getColumnIndex("month")));
//						alarmDTO.setMonthNum(Integer.parseInt(c1.getString(c1
//								.getColumnIndex("month_num"))));
//						alarmDTO.setYear(Integer.parseInt(c1.getString(
//								c1.getColumnIndex("year")).trim()));
//						alarmDTO.setHour(Integer.parseInt(c1.getString(
//								c1.getColumnIndex("hour")).trim()));
//						alarmDTO.setMinute(Integer.parseInt((c1.getString(c1
//								.getColumnIndex("minute")).trim())));
//						alarmDTO.setMaxLeftMinute(Integer.parseInt((c1.getString(c1
//								.getColumnIndex("max_left_minute")).trim())));
//						alarmDTO.setMaxLeftSecond(Integer.parseInt((c1.getString(c1
//								.getColumnIndex("max_left_second")).trim())));
//						alarmDTO.setEndDatestring((c1.getString(c1
//								.getColumnIndex("end_date")).trim()));
//					}
//				}
//				c1.close();// closing connection
//			} catch (Exception e) {
//				System.out.println("DATABASE ERROR 8 " + e);
//				e.printStackTrace();
//				return null;
//			}
//
//			return alarmDTO;
//	}

		/**
		 * @param alarmValues
		 * @param id 
		 * @return
		 */
		public boolean updateAlarm(ContentValues alarmValues, int id) {
			try {
				if (mDb.isOpen()) {
					mDb.close();
				}
				mDb = mDbHelper.getWritableDatabase();
				mDb.update("alarm_tbl", alarmValues, "id = ?", new String[] { String.valueOf(id) });
			} catch (Exception e) {

				System.out.println("DATABASE ERROR 5 " + e);
				e.printStackTrace();
				return false;
			}
			return true;
		}

    public ArrayList<EventList.ListData> getEventListView() {
        ArrayList<EventList.ListData> localArrayList = new ArrayList<EventList.ListData>();

        EventList.ListData listData ;

        Cursor c1 = null;
        String eventName;
        String id;
        boolean isPosition;
        String type;
        try {

            if (mDb.isOpen()) {
                mDb.close();
            }
            mDb = mDbHelper.getWritableDatabase();

            c1 = mDb.rawQuery("select * from event_tbl ", null);
            if (c1 != null && c1.getCount() > 0) {
                if (c1.moveToFirst()) {
                    do {


                        id  = c1.getString(c1.getColumnIndex("id"));
                        eventName = c1.getString(c1
                                .getColumnIndex("event_name")) ;


                        String s = c1.getString(c1
                                .getColumnIndex("is_position")).trim();
                        type = c1.getString(c1
                                .getColumnIndex("type")).trim();

                        if (s.equalsIgnoreCase("1") ||s.equalsIgnoreCase("true") ){
                            isPosition = true;
                        } else {
                           isPosition = false;
                        }


                        listData = new EventList().new ListData(id, eventName, isPosition,type);

                        localArrayList.add(listData);


                    } while (c1.moveToNext());
                }/*tasknumber.setVisibility(8);

            } else {
                tasknumber.setVisibility(0);
                tasknumber.setText("No Task Found");
            }*/
            }
            c1.close();// closing connection

        } catch (Exception e) {
            System.out.println("DATABASE ERROR 8 " + e);
            e.printStackTrace();
            return null;
        }
        return localArrayList;

    }


    public Long saveEventDetail(ContentValues contentValues) {
        try {
            Long id = mDb.insert("event_detail_tbl", null, contentValues);

            Log.d("Save category", "informationsaved");
            return id;

        } catch (Exception ex) {
            Log.d("Save event", ex.toString());
            ex.printStackTrace();
            return -1L;
        }
    }

    public ArrayList<EventDataList.ListData> getEventDetailList(int id1,String eventType) {
        ArrayList<EventDataList.ListData> localArrayList = new ArrayList<EventDataList.ListData>();
        EventDataList.ListData listData ;
        Cursor c1 = null;
        String id = "";
        String date= "";
        String time= "";
        String latitude= "";
        String longitude= "";
        String situation= "";
        String numeric = "";
        String text = "";

        try {
            if (mDb.isOpen()) {
                mDb.close();
            }
            mDb = mDbHelper.getWritableDatabase();

            c1 = mDb.rawQuery("select * from event_detail_tbl where event_id = '"+ id1 +"' ", null);
            if (c1 != null && c1.getCount() > 0) {
                if (c1.moveToFirst()) {
                    do {
                        id  = c1.getString(c1.getColumnIndex("id"));
                        date = c1.getString(c1
                                .getColumnIndex("date")) ;
                        time = c1.getString(c1
                                .getColumnIndex("time")) ;
                        latitude = c1.getString(c1
                                .getColumnIndex("latitude")) ;
                        longitude = c1.getString(c1
                                .getColumnIndex("longitude")) ;
                        if(eventType.equalsIgnoreCase(mContext.getResources().getString(R.string.situation))){
                            situation = c1.getString(c1
                                    .getColumnIndex("situation")) ;
                        }else if(eventType.equalsIgnoreCase(mContext.getResources().getString(R.string.numerical))){
                            numeric = c1.getString(c1
                                    .getColumnIndex("numeric")) ;
                        } else if(eventType.equalsIgnoreCase(mContext.getResources().getString(R.string.text))){
                            text = c1.getString(c1
                                    .getColumnIndex("text")) ;
                        }


                        listData = new EventDataList().new ListData(id, date, time,latitude,longitude,situation,numeric,text);

                        localArrayList.add(listData);


                    } while (c1.moveToNext());
                }
            }
            c1.close();// closing connection

        } catch (Exception e) {
            System.out.println("DATABASE ERROR 8 " + e);
            e.printStackTrace();
            return null;
        }
        return localArrayList;

    }

    public void deleteEvent() {
        String eventName = "";
        Cursor c1 = null;
        try {

            c1 = mDb.rawQuery("delete FROM event_tbl where id in( SELECT max(id) FROM event_tbl)", null);
            if (c1 != null && c1.getCount() > 0) {
                if (c1.moveToFirst()) {
//					do {
//                    eventName = c1.getString(c1.getColumnIndex("event_name"));
//					} while (c1.moveToNext());
                }
            }
            c1.close();// closing connection


        } catch (Exception e) {
            System.out.println("DATABASE ERROR 8 " + e);
            e.printStackTrace();
//            return eventName;
        }
//        return eventName;
    }

    public void saveState(ArrayList<ContentValues> contentValueses) {
        try {
            Iterator<ContentValues> contentValuesIterator = contentValueses.iterator();
            while (contentValuesIterator.hasNext()){
                try {
                    Long id = mDb.insert("event_state_tbl", null, contentValuesIterator.next());

                    Log.d("Save state", "informationsaved");
//                    return id;

                } catch (Exception ex) {
                    Log.d("Save state", ex.toString());
                    ex.printStackTrace();
//                    return -1L;
                }
            }

        }
        catch (Exception e){

            Log.d("Save event", e.toString());
            e.printStackTrace();
        }

    }

    public ArrayList<String> getState(int id1) {
        ArrayList<String> localArrayList = new ArrayList<String>();
        Cursor c1 = null;
        try {

            if (mDb.isOpen()) {
                mDb.close();
            }
            mDb = mDbHelper.getWritableDatabase();

            c1 = mDb.rawQuery("select * from event_state_tbl where event_id = '" + id1 + "'", null);
            if (c1 != null && c1.getCount() > 0) {
                if (c1.moveToFirst()) {
                    do {
//                        id  = c1.getString(c1.getColumnIndex("id"));
                        String state = c1.getString(c1
                                .getColumnIndex("state")) ;

                        if (!state.equals("")){
                            localArrayList.add(state);
                        } else {
                        }
                    } while (c1.moveToNext());
                }
            }
            c1.close();// closing connection

        } catch (Exception e) {
            System.out.println("DATABASE ERROR 10 " + e);
            e.printStackTrace();
            return null;
        }
        return localArrayList;
    }
}
