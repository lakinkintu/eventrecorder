package com.example.eventrecorder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

@SuppressLint("NewApi")
public class EventDataList
  extends Activity
{
  ListAdapter adapter;
  ListView custom_list;
  
  @SuppressLint("NewApi")
public void onActivityCreated(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.custom_list = ((ListView)findViewById(2131034273));
    @SuppressWarnings("rawtypes")
	ArrayList localArrayList = new ArrayList();
 
    ListAdapter localListAdapter = new ListAdapter(localArrayList, this);
    this.custom_list.setAdapter(localListAdapter);
    this.custom_list.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      @SuppressLint("NewApi")
	public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {

      }
    });
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
//    return paramLayoutInflater.inflate(2130903076, paramViewGroup, false);
    return paramLayoutInflater.inflate(R.layout.event_detail_list, paramViewGroup, false);
  }
  
  public class ListData
  {
	  String id="";
      String date="";
      String time="";
      String latitude="";
      String longitude="";
      String situation="";
      String numeric="";
      String text="";

      public String getSituation() {
          return situation;
      }

      public void setSituation(String situation) {
          this.situation = situation;
      }

      public String getNumeric() {
          return numeric;
      }

      public void setNumeric(String numeric) {
          this.numeric = numeric;
      }

      public String getText() {
          return text;
      }

      public void setText(String text) {
          this.text = text;
      }

      public String getLatitude() {
          return latitude;
      }

      public void setLatitude(String latitude) {
          this.latitude = latitude;
      }

      public String getLongitude() {
          return longitude;
      }

      public void setLongitude(String longitude) {
          this.longitude = longitude;
      }

      public String getTime() {
          return time;
      }

      public void setTime(String time) {
          this.time = time;
      }

      public String getDate() {
          return date;
      }

      public void setDate(String date) {
          this.date = date;
      }

      public void setId(String id) {
          this.id = id;
      }




	public String getId() {
		return id;
	}

      public ListData(String id, String date, String time, String latitude, String longitude, String situation, String numeric, String text) {
          this.id = id;
          this.date = date;
          this.time = time;
          this.latitude = latitude;
          this.longitude = longitude;
          this.situation = situation;
          this.numeric = numeric;
          this.text = text;
      }
  }
}


