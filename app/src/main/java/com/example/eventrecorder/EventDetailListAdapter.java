package com.example.eventrecorder;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class EventDetailListAdapter extends BaseAdapter {
    Context context;
    List<EventDataList.ListData> list = new ArrayList();
    boolean positionBoolean;
    String eventType = "";

    EventDetailListAdapter(List<EventDataList.ListData> paramList, Context paramContext) {
        this.list = paramList;
        this.context = paramContext;
    }

    EventDetailListAdapter(List<EventDataList.ListData> paramList, Context paramContext, boolean positionBoolean) {
        this.list = paramList;
        this.context = paramContext;
        this.positionBoolean = positionBoolean;
    }

    public EventDetailListAdapter(List<EventDataList.ListData> paramList, Context paramContext, boolean positionBoolean, String eventType) {
        this.list = paramList;
        this.context = paramContext;
        this.positionBoolean = positionBoolean;
        this.eventType = eventType;
    }

    public int getCount() {
        return this.list.size();
    }

    public Object getItem(int paramInt) {
        return this.list.get(paramInt);
    }

    public long getItemId(int paramInt) {
        return paramInt;
    }

    public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
        TextView localTextId;
        TextView localTextViewDate;
        TextView localTextViewTime;
        TextView localTextViewLatitude;
        TextView localTextViewLongitude;
        TextView localTextViewSituation;
        TextView localTextViewNumeric;
        TextView localTextViewText;
        try {


            if (paramView == null) {
                if (eventType.equalsIgnoreCase(context.getResources().getString(R.string.situation))) {
                    if (positionBoolean) {
                        paramView = ((LayoutInflater) this.context
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                                R.layout.event_detail_list, paramViewGroup, false);
                    } else {

                        paramView = ((LayoutInflater) this.context
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                                R.layout.event_detail_list_without_pos, paramViewGroup, false);
                    }

                } else if (eventType.equalsIgnoreCase(context.getResources().getString(R.string.numerical))) {
                    if (positionBoolean) {
                        paramView = ((LayoutInflater) this.context
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                                R.layout.event_detail_list, paramViewGroup, false);
                    } else {

                        paramView = ((LayoutInflater) this.context
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                                R.layout.event_detail_list_without_pos, paramViewGroup, false);
                    }
                } else if (eventType.equalsIgnoreCase(context.getResources().getString(R.string.text))) {
                    if (positionBoolean) {
                        paramView = ((LayoutInflater) this.context
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                                R.layout.event_detail_list, paramViewGroup, false);
                    } else {

                        paramView = ((LayoutInflater) this.context
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
                                R.layout.event_detail_list_without_pos, paramViewGroup, false);
                    }
                }


            }
            if (paramInt % 2 == 0) {
                paramView.setBackgroundColor(Color.rgb(238, 238, 238));
            } else {
                paramView.setBackgroundColor(Color.rgb(139, 197, 246));
            }

            for (; ; ) {
                localTextId = (TextView) paramView.findViewById(R.id.id);
                localTextViewDate = (TextView) paramView
                        .findViewById(R.id.date);
                localTextViewTime = (TextView) paramView
                        .findViewById(R.id.time);
                localTextViewLatitude = (TextView) paramView
                        .findViewById(R.id.latitude);
                localTextViewLongitude = (TextView) paramView
                        .findViewById(R.id.longitude);
                localTextViewSituation = (TextView) paramView
                        .findViewById(R.id.situation);
                localTextViewNumeric = (TextView) paramView
                        .findViewById(R.id.numeric);
                localTextViewText = (TextView) paramView
                        .findViewById(R.id.text);

                EventDataList.ListData localListData = this.list
                        .get(paramInt);

                localTextId.setText(localListData.getId());
                localTextViewDate.setText(localListData.getDate());
                localTextViewTime.setText(localListData.getTime());
                if (positionBoolean) {
                    localTextViewLatitude.setText(localListData.getLatitude());
                    localTextViewLongitude.setText(localListData.getLongitude());
                }
                if (eventType.equalsIgnoreCase(context.getResources().getString(R.string.situation))) {
                    localTextViewSituation.setText(localListData.getSituation());
                    localTextViewSituation.setVisibility(View.VISIBLE);
                } else if (eventType.equalsIgnoreCase(context.getResources().getString(R.string.numerical))) {
                    localTextViewNumeric.setText(localListData.getNumeric());
                    localTextViewNumeric.setVisibility(View.VISIBLE);
                } else if (eventType.equalsIgnoreCase(context.getResources().getString(R.string.text))) {
                    localTextViewText.setText(localListData.getText());
                    localTextViewText.setVisibility(View.VISIBLE);
                }

                return paramView;
                // paramView.setBackgroundResource(2130837599);
            }
        } catch (Exception e) {
            e.printStackTrace();
            showToast(e.getMessage());
            return paramView;
        }
    }

    private void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT)
                .show();
    }
}

