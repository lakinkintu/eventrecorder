package com.example.eventrecorder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Comparator;

@SuppressLint("NewApi")
public class EventList
  extends Activity
{
  ListAdapter adapter;
  ListView custom_list;
  
  @SuppressLint("NewApi")
public void onActivityCreated(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    this.custom_list = ((ListView)findViewById(2131034273));
    @SuppressWarnings("rawtypes")
	ArrayList localArrayList = new ArrayList();
 
    ListAdapter localListAdapter = new ListAdapter(localArrayList, this);
    this.custom_list.setAdapter(localListAdapter);
    this.custom_list.setOnItemClickListener(new AdapterView.OnItemClickListener()
    {
      @SuppressLint("NewApi")
	public void onItemClick(AdapterView<?> paramAnonymousAdapterView, View paramAnonymousView, int paramAnonymousInt, long paramAnonymousLong)
      {

      }
    });
  }
  
  public View onCreateView(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
//    return paramLayoutInflater.inflate(2130903076, paramViewGroup, false);
    return paramLayoutInflater.inflate(R.layout.single_list, paramViewGroup, false);
  }
  
  public class ListData implements Comparable
  {
	private String id;
      private String eventName;


      private boolean isPosition;
      private String type;

      public ListData(String id, String eventName, boolean isPosition, String type) {
          this.id = id;
          this.eventName = eventName;
          this.isPosition = isPosition;
          this.type = type;
      }

      public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


      public String getEventName() {
          return eventName;
      }

      public void setEventName(String eventName) {
          this.eventName = eventName;
      }

      public boolean isPosition() {
          return isPosition;
      }

      public void setPosition(boolean isPosition) {
          this.isPosition = isPosition;
      }




      public String getType() {
          return type;
      }

      public void setType(String type) {
          this.type = type;
      }

      @Override
      public int compareTo(Object another) {
          if (!(another instanceof ListData))
              throw new ClassCastException();

          ListData e = (ListData) another;

          return eventName.compareTo(e.getEventName());
      }
//       class IdComparator implements Comparator {
//          public int compare(Object o1, Object o2) {
//              if (!(o1 instanceof ListData) || !(o2 instanceof ListData))
//                  throw new ClassCastException();
//
//              ListData e1 = (ListData) o1;
//              ListData e2 = (ListData) o2;
//
//              return (int) (Integer.parseInt(e1.getId().trim()) - Integer.parseInt(e2.getId().trim()));
//          }
//      }

  }
   static class IdComparator implements Comparator {
        public int compare(Object o1, Object o2) {
            if (!(o1 instanceof ListData) || !(o2 instanceof ListData))
                throw new ClassCastException();

            ListData e1 = (ListData) o1;
            ListData e2 = (ListData) o2;

            return (int) (Integer.parseInt(e1.getId().trim()) - Integer.parseInt(e2.getId().trim()));
        }
    }
}


