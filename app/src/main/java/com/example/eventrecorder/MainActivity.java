package com.example.eventrecorder;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;


public class MainActivity  extends TabActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);






//
//        ActionBar.NavigationMode = ActionBarNavigationMode.Tabs;
//        SetContentView(Resource.Layout.Main);
//
//        ActionBar.Tab tab = ActionBar.NewTab();
//        tab.SetText(Resources.GetString(Resource.String.tab1_text));
//        tab.SetIcon(Resource.Drawable.tab1_icon);
//        tab.TabSelected += (sender, args) => {
//            // Do something when tab is selected
//        }
//        ActionBar.AddTab(tab);
//
//        tab = ActionBar.NewTab();
//        tab.SetText(Resources.GetString(Resource.String.tab2_text));
//        tab.SetIcon(Resource.Drawable.tab2_icon);
//        tab.TabSelected += (sender, args) => {
//            // Do something when tab is selected
//        }
//        ActionBar.AddTab(tab);







        TabHost tabHost = getTabHost();

        // Tab for Photos
        TabHost.TabSpec photospec = tabHost.newTabSpec("Grab");
        // setting Title and Icon for the Tab
//        photospec.setIndicator("Photos", getResources().getDrawable(R.drawable.icon_photos_tab));
        photospec.setIndicator("Grab");
        Intent photosIntent = new Intent(this, GrabActivity.class);
        photospec.setContent(photosIntent);

        // Tab for Songs
        TabHost.TabSpec songspec = tabHost.newTabSpec("Report");
//        songspec.setIndicator("Songs", getResources().getDrawable(R.drawable.icon_songs_tab));
        songspec.setIndicator("Report");
        Intent songsIntent = new Intent(this, ReportActivity.class);
        songspec.setContent(songsIntent);

        // Tab for Videos
        TabHost.TabSpec videospec = tabHost.newTabSpec("Settings");
//        videospec.setIndicator("Videos", getResources().getDrawable(R.drawable.icon_videos_tab));
        videospec.setIndicator("Settings");
        Intent videosIntent = new Intent(this, SettingsActivity.class);
        videospec.setContent(videosIntent);

        // Adding all TabSpec to TabHost
        tabHost.addTab(photospec); // Adding photos tab
        tabHost.addTab(songspec); // Adding songs tab
        tabHost.addTab(videospec); // Adding videos tab
    }

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//    }
//
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
