package com.example.eventrecorder;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ListAdapter extends BaseAdapter {
	Context context;
	List<EventList.ListData> list = new ArrayList();

	ListAdapter(List<EventList.ListData> paramList, Context paramContext) {
		this.list = paramList;
		this.context = paramContext;
	}

	public int getCount() {
		return this.list.size();
	}

	public Object getItem(int paramInt) {
		return this.list.get(paramInt);
	}

	public long getItemId(int paramInt) {
		return paramInt;
	}

	public View getView(int paramInt, View paramView, ViewGroup paramViewGroup) {
		TextView localTextId;
		TextView localTextViewEventName;
		TextView localTextViewIsPosition;
        TextView localTextViewType;

		if (paramView == null) {
			paramView = ((LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(
					R.layout.single_list, paramViewGroup, false);
            paramView.setBackgroundColor(Color.rgb(139, 197, 246));
        }
		if (paramInt % 2 == 0) {
			 paramView.setBackgroundColor(Color.rgb(238,238,238));
		}else {
            paramView.setBackgroundColor(Color.rgb(192,192,192));
        }

		for (;;) {
			localTextId = (TextView) paramView.findViewById(R.id.id);
			localTextViewEventName = (TextView) paramView
					.findViewById(R.id.event_name);
            localTextViewIsPosition = (TextView) paramView
					.findViewById(R.id.is_position);
            localTextViewType = (TextView) paramView
                    .findViewById(R.id.type);
			EventList.ListData localListData = (EventList.ListData) this.list
					.get(paramInt);

			localTextId.setText(localListData.getId());
            localTextViewEventName.setText(localListData.getEventName());
            localTextViewIsPosition.setText(String.valueOf(localListData.isPosition()));
            localTextViewType.setText(String.valueOf(localListData.getType()));


			return paramView;
			// paramView.setBackgroundResource(2130837599);
		}
		
	}
    private void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT)
                .show();
    }
}

