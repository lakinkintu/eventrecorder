package com.example.eventrecorder;

import android.app.Activity;
import android.content.ContentValues;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.dao.TestAdapter;
import com.example.utility.KeyboardUtility;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by C-13 on 6/1/2015.
 */
public class SettingsActivity extends Activity {
    Button newEvent;
    EditText eventName;

    LinearLayout situationLayout;

    LinearLayout numericalLayout;
    EditText state1;
    EditText state2;
    EditText state3;
    EditText state4;

    RadioGroup radioGroup;
    RadioButton situationRBtn;
    RadioButton numericalRBtn;
    RadioButton textRBtn;
    CheckBox isPosition;
    CheckBox isPositionNumText;
    Button create;
    private TestAdapter mDbHelper;
    private EditText eventNameNumText;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity_layout);
        new KeyboardUtility().setupUI(findViewById(R.id.parent), this);
        initVar();

    }

    private void initVar() {
        situationLayout = (LinearLayout) findViewById(R.id.situation_layout);

        numericalLayout = (LinearLayout) findViewById(R.id.numerical_layout);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.situation_rbtn) {
                    situationLayout.setVisibility(View.VISIBLE);
                    numericalLayout.setVisibility(View.GONE);
                } else {
                    situationLayout.setVisibility(View.GONE);
                    numericalLayout.setVisibility(View.VISIBLE);

                }
            }
        });
        situationRBtn = (RadioButton) findViewById(R.id.situation_rbtn);
        numericalRBtn = (RadioButton) findViewById(R.id.numerical_rbtn);
        textRBtn = (RadioButton) findViewById(R.id.text_rbtn);


        mDbHelper = new TestAdapter(this);
        mDbHelper.createDatabase();
        newEvent = (Button) findViewById(R.id.new_event);
        newEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
        eventName = (EditText) findViewById(R.id.event_name);
        eventNameNumText = (EditText) findViewById(R.id.event_name_num_text);
        isPositionNumText = (CheckBox) findViewById(R.id.position_num_text_checkbox);
        state1 = (EditText) findViewById(R.id.state_1);
        state2 = (EditText) findViewById(R.id.state_2);
        state3 = (EditText) findViewById(R.id.state_3);
        state4 = (EditText) findViewById(R.id.state_4);

        isPosition = (CheckBox) findViewById(R.id.position_checkbox);
        create = (Button) findViewById(R.id.create);
//        create.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                int checkedRadioButtonId = radioGroup.getCheckedRadioButtonId();
//                switch (checkedRadioButtonId) {
//                    case R.id.situation_rbtn:
//                        saveSituation();
//                        break;
//                    case R.id.numerical_rbtn:
//                        saveNumerical();
//                        break;
//                    case R.id.text_rbtn:
//                        saveText();
//
//                        break;
//                    default:
//                        saveSituation();
//
//                }
//
//            }
//        });
    }

    private void saveText() {
        if (eventNameNumText.getText().toString().trim().equals("")) {
            showToast(getResources().getString(R.string.empty_event_name));
        } else {

            mDbHelper.createDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("event_name", eventNameNumText.getText().toString());
            contentValues.put("type",((RadioButton)findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString());
            contentValues.put("is_position", isPositionNumText.isChecked());
            mDbHelper.open();
            Long aLong = mDbHelper.saveEvent(contentValues);
            if (aLong > 0) {
                showToast(getResources().getString(R.string.event_saved));
                saveState(aLong);
            } else {
                showToast(getResources().getString(R.string.event_saved_failed));
            }
            mDbHelper.close();
        }
    }

    private void saveNumerical() {
        if (eventNameNumText.getText().toString().trim().equals("")) {
            showToast(getResources().getString(R.string.empty_event_name));
        } else {

            mDbHelper.createDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("event_name", eventNameNumText.getText().toString());
            contentValues.put("type",((RadioButton)findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString());
            contentValues.put("is_position", isPositionNumText.isChecked());
            mDbHelper.open();
            Long aLong = mDbHelper.saveEvent(contentValues);
            if (aLong > 0) {
                showToast(getResources().getString(R.string.event_saved));
                saveState(aLong);
            } else {
                showToast(getResources().getString(R.string.event_saved_failed));
            }
            mDbHelper.close();
        }
    }

    private void saveSituation() {
        if (eventName.getText().toString().trim().equals("")) {
            showToast(getResources().getString(R.string.empty_event_name));
        } else {

            mDbHelper.createDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("event_name", eventName.getText().toString());
            contentValues.put("type",((RadioButton)findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString());
            contentValues.put("is_position", isPosition.isChecked());
            mDbHelper.open();
            Long aLong = mDbHelper.saveEvent(contentValues);
            if (aLong > 0) {
                showToast(getResources().getString(R.string.event_saved));
                saveState(aLong);
            } else {
                showToast(getResources().getString(R.string.event_saved_failed));
            }
            mDbHelper.close();
        }
    }


    private void saveState(long aLong) {
        mDbHelper.createDatabase();
        ArrayList<String> stateList = new ArrayList<String>();
        stateList.add(state1.getText().toString());
        stateList.add(state2.getText().toString());
        stateList.add(state3.getText().toString());
        stateList.add(state4.getText().toString());
        ArrayList<ContentValues> contentValueses = new ArrayList<ContentValues>();
        Iterator<String> stringIterator = stateList.iterator();
        while (stringIterator.hasNext()) {
            ContentValues contentValues = new ContentValues();
            contentValues.put("event_id", (int) aLong);
            contentValues.put("state", stringIterator.next());
            contentValueses.add(contentValues);
        }

        mDbHelper.open();
        mDbHelper.saveState(contentValueses);
        mDbHelper.close();
    }

    private void showToast(String message) {
        Toast.makeText(SettingsActivity.this, message, Toast.LENGTH_SHORT)
                .show();
    }
    public void onClick(View v) {

        int checkedRadioButtonId = radioGroup.getCheckedRadioButtonId();
        switch (checkedRadioButtonId) {
            case R.id.situation_rbtn:
                saveSituation();
                break;
            case R.id.numerical_rbtn:
                saveNumerical();
                break;
            case R.id.text_rbtn:
                saveText();

                break;
            default:
                saveSituation();

        }

    }
}