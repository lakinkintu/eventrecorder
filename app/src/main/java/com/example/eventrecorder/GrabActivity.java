package com.example.eventrecorder;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.method.DigitsKeyListener;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dao.TestAdapter;
import com.example.gps.GPSTracker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

/**
 * Created by C-13 on 6/1/2015.
 */
public class GrabActivity extends Activity {
    private GridView gridView;
    //    private EventAdapter adapter;
    ListAdapter adapter;
    private TestAdapter mDbHelper;
    TextView byNameTextView;
    TextView byLatestTextView;

    ArrayList<EventList.ListData> localArrayList = new ArrayList<EventList.ListData>();

    private Button deleteRecent;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grab_activity_layout);
        initVar();
    }

    private void initVar() {

        mDbHelper = new TestAdapter(getBaseContext());
       byNameTextView   =  (TextView)findViewById(R.id.by_name);
        byNameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortGridItem("name");

            }
        });
       byLatestTextView = (TextView)findViewById(R.id.by_latest);
        byLatestTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortGridItem(getResources().getString(R.string.latest));
//                Collections.sort(localArrayList, new EventList.IdComparator());
//                adapter = new ListAdapter(localArrayList, getBaseContext());
//                // set tasklist adapter to listview
//                gridView.setAdapter(adapter);
            }
        });

        deleteRecent = (Button)findViewById(R.id.delete_recent);
        deleteRecent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDbHelper.open();
                String recentData = mDbHelper.getRecentData();
                if(!"".equalsIgnoreCase(recentData) && recentData != null){
                    AlertDialog.Builder builder = new AlertDialog.Builder(GrabActivity.this);
                    builder.setMessage("Do you want to remove " + recentData
                            + "?");
                    builder.setCancelable(false);
                    builder.setPositiveButton("Yes",
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    deleteEvent();

                                    startActivity(new Intent(
                                            GrabActivity.this,
                                            MainActivity.class));
                                    finish();

                                }

                                private void deleteEvent() {
                                    mDbHelper.open();
                                    mDbHelper.deleteEvent();
                                    mDbHelper.close();

                                }
                            });
                    builder.setNegativeButton("No",
                            new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    dialog.cancel();
                                }
                            });
                    // Create and show the dialog
                    builder.show();
                }
                mDbHelper.close();
            }
        });
        gridView = (GridView) findViewById(R.id.gridview);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String type = (((TextView) view.findViewById(R.id.type)).getText().toString());
                if(type.equalsIgnoreCase(getResources().getString(R.string.situation))){
                    showSituation(view,type);
                } else if (type.equalsIgnoreCase(getResources().getString(R.string.numerical))){
                    showNumericalDialog(view,type);
                    showToast("numerical");
                } else if (type.equalsIgnoreCase(getResources().getString(R.string.text))){
                    showTextDialog(view,type);
                    showToast("text");
                }

            }
        });
        mDbHelper = new TestAdapter(this);
        mDbHelper.createDatabase();

        setTaskListView();
    }

    private void sortGridItem(String bySort) {
        if (bySort.equalsIgnoreCase(getResources().getString(R.string.name))){
            Collections.sort(localArrayList);

        } else {
            Collections.sort(localArrayList, new EventList.IdComparator());
        }
        adapter = new ListAdapter(localArrayList, getBaseContext());
        // set tasklist adapter to listview
        gridView.setAdapter(adapter);
    }

    private void showTextDialog(final View view, final String type) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle(getResources().getString(R.string.enter_number));
//        alert.setMessage("New age?");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        input.setGravity(Gravity.CENTER);
        input.setSingleLine(true);
        alert.setView(input);

        input.setText("text");


        alert.setPositiveButton(getResources().getString(R.string.save),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        showToast(input.getText().toString());

                        saveEventDetail(view,input.getText().toString(), type);
                    }
                });
//
//        alert.setNegativeButton("Keep", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//                // do nothing
//            }
//        }
//    );
        alert.show();
    }

    private void showNumericalDialog(final View view, final String type) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle(getResources().getString(R.string.enter_number));
//        alert.setMessage("New age?");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        input.setGravity(Gravity.CENTER);
        input.setSingleLine(true);
        alert.setView(input);

        input.setText("3");
        input.setFilters(new InputFilter[] {
                // Maximum 10 characters.
                new InputFilter.LengthFilter(10),
                // Digits only.
                DigitsKeyListener.getInstance(),  // Not strictly needed, IMHO.
        });

// Digits only & use numeric soft-keyboard.
        input.setKeyListener(DigitsKeyListener.getInstance());

        alert.setPositiveButton(getResources().getString(R.string.save),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        showToast(input.getText().toString());

                        saveEventDetail(view,input.getText().toString(),type);
                    }
                });
//
//        alert.setNegativeButton("Keep", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int whichButton) {
//                // do nothing
//            }
//        }
//    );
        alert.show();
    }

    private void showSituation(final View view, final String type) {

        int id1 = Integer.parseInt(((TextView) view.findViewById(R.id.id)).getText().toString());
        mDbHelper.open();
        ArrayList<String> state = mDbHelper.getState(id1);
        if (state == null){

        }else
        if (!state.isEmpty() ){
//                    if (!state.isEmpty()){
//            CharSequence colors[] = new CharSequence[] {};
            String[] stateArr = new String[state.size()];
            stateArr = state.toArray(stateArr);
            final AlertDialog.Builder builder = new AlertDialog.Builder(GrabActivity.this);
            builder.setTitle("Pick a State");

            final String[] finalStateArr = stateArr;
            builder.setItems(stateArr, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                  showToast((finalStateArr[which].toString()));

                    saveEventDetail(view,finalStateArr[which].toString(), type);
                }
            });
            builder.show();

//                    }
        }

        mDbHelper.close();

    }

    private void saveEventDetail(View view, String input, String type) {

        try {
            mDbHelper.open();
            int id1 = Integer.parseInt(((TextView) view.findViewById(R.id.id)).getText().toString());
            GPSTracker gpsTracker = new GPSTracker(getBaseContext());
            int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
            int month = Calendar.getInstance().get(Calendar.MONTH)+1;
            int year = Calendar.getInstance().get(Calendar.YEAR);
            String date = day + "/" + month + "/" + year ;
            ContentValues contentValues = new ContentValues();
            contentValues.put("event_id",id1);
            contentValues.put("date", date);
            if (type.equalsIgnoreCase(getResources().getString(R.string.situation))){
                contentValues.put("situation", input);
            } else if (type.equalsIgnoreCase(getResources().getString(R.string.numerical))){
                contentValues.put("numeric", input);
            } else if (type.equalsIgnoreCase(getResources().getString(R.string.text))){
                contentValues.put("text", input);
            }

            contentValues.put("time",Calendar.getInstance().get(Calendar.HOUR)+":"+Calendar.getInstance().get(Calendar.MINUTE)+":"+Calendar.getInstance().get(Calendar.SECOND));
            contentValues.put("latitude", Location.convert(gpsTracker.getLatitude(), Location.FORMAT_SECONDS));
            contentValues.put("longitude",Location.convert(gpsTracker.getLongitude(), Location.FORMAT_SECONDS));
            Long aLong = mDbHelper.saveEventDetail(contentValues);
            if (aLong > 0) {
                showToast(getResources().getString(R.string.event_saved));
            } else {
                showToast(getResources().getString(R.string.event_saved_failed));
            }
            mDbHelper.close();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void setTaskListView() {

        mDbHelper.open();
        localArrayList = mDbHelper.getEventListView();
        if (localArrayList.isEmpty()) {
//            message.setVisibility(0);
            // isTaskExist.setText("No job Found");
//            message.setText("Geen baan gevonden");
        } else if (localArrayList == null) {
//            message.setVisibility(0);
//            message.setText("");
        } else {
            // make adapter of task list
            adapter = new ListAdapter(localArrayList, getBaseContext());
            // set tasklist adapter to listview

            gridView.setAdapter(adapter);
        }

        mDbHelper.close();

    }

    @Override
    protected void onResume() {
        super.onResume();
        setTaskListView();
    }

    private void showToast(String message) {
        Toast.makeText(getBaseContext(), message, Toast.LENGTH_SHORT)
                .show();
    }
}