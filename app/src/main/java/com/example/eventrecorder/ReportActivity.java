package com.example.eventrecorder;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dao.TestAdapter;
import com.example.dto.EventDetailDTO;
import com.example.fileSave.SimpleFileDialog;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import au.com.bytecode.opencsv.CSVWriter;

/**
 * Created by C-13 on 6/1/2015.
 */
public class  ReportActivity extends Activity {
    private TestAdapter mDbHelper;
    ArrayList<EventList.ListData> localEventList = new ArrayList<EventList.ListData>();
    ArrayList<EventDataList.ListData> localEventDataList = new ArrayList<EventDataList.ListData>();
    private ListAdapter adapter;
    private EventDetailListAdapter eventDetailListAdapter;
    ListView eventListView;
    ListView eventDetaListView;
    private Button exportButton;
    int selectedEvent = 0;
    String seletedEventName = "";
    boolean isPosition;
    private String type;
     ViewGroup headerWithoutPos;
    ViewGroup header;
    ViewGroup addedHeader = null;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_activity_layout);
        initVar();
    }

    private void initVar() {

        headerWithoutPos = (ViewGroup)getLayoutInflater().inflate(R.layout.header_withot_pos, eventDetaListView, false);
        header = (ViewGroup)getLayoutInflater().inflate(R.layout.header, eventDetaListView, false);
        mDbHelper = new TestAdapter(this);
        mDbHelper.createDatabase();
        exportButton = (Button) findViewById(R.id.export_button);
        exportButton.setOnClickListener(new View.OnClickListener()
        {
            String m_chosen;
            @Override
            public void onClick(View v) {

                /////////////////////////////////////////////////////////////////////////////////////////////////
                //Create FileOpenDialog and register a callback
                /////////////////////////////////////////////////////////////////////////////////////////////////
                SimpleFileDialog FolderChooseDialog =  new SimpleFileDialog(ReportActivity.this, "FolderChoose",
                        new SimpleFileDialog.SimpleFileDialogListener()
                        {
                            @Override
                            public void onChosenDir(String chosenDir)
                            {
                                // The code in this function will be executed when the dialog OK button is pushed
                                m_chosen = chosenDir;
                                Toast.makeText(ReportActivity.this, "Chosen FileOpenDialog File: " +
                                        m_chosen, Toast.LENGTH_LONG).show();
                                if(!m_chosen.equals("") && m_chosen != null){
                                    saveCSV(m_chosen);
                                }
                            }
                        });

                FolderChooseDialog.chooseFile_or_Dir();

                /////////////////////////////////////////////////////////////////////////////////////////////////

            }
        });
//        exportButton.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v) {
//                mDbHelper.open();
//                ArrayList arrayList = mDbHelper.getEventDetailDTOList();
//                updateDateValues(arrayList);
//                mDbHelper.close();
//
//            }
//        });
        eventListView = (ListView) findViewById(R.id.event_list);

        eventListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               int id1 = Integer.parseInt(((TextView) view.findViewById(R.id.id)).getText().toString());
               seletedEventName = ((TextView) view.findViewById(R.id.event_name)).getText().toString();
                isPosition = Boolean.valueOf(((TextView) view.findViewById(R.id.is_position)).getText().toString());
                type = ((TextView) view.findViewById(R.id.type)).getText().toString();
//                view.setBackgroundColor(Color.rgb(226,243,255));

                selectedEvent = id1;

                setEventDataListView(id1,type);
            }
        });
        eventDetaListView = (ListView)findViewById(R.id.event_data_list);
        setEventListView();
//        setEventDataListView();
    }

    private void saveCSV(String m_chosen) {
        mDbHelper.open();
        ArrayList arrayList = mDbHelper.getEventDetailDTOList(selectedEvent,seletedEventName);
        updateDateValues(arrayList,m_chosen);
        mDbHelper.close();
    }

    private void setEventListView() {
        mDbHelper.open();
        // alarmList = mDbHelper.getAlarmList();
        localEventList = mDbHelper.getEventListView();
        if (localEventList.isEmpty()) {
//            message.setVisibility(0);
//            // isTaskExist.setText("No job Found");
//            message.setText("Geen baan gevonden");
        } else if (localEventList == null) {
//            message.setVisibility(0);
//            message.setText("");
        } else {
            // make adapter of task list
            adapter = new ListAdapter(localEventList, getBaseContext());
            // set tasklist adapter to listview

            eventListView.setAdapter(adapter);
        }
    }

    private void setEventDataListView(int id, String type) {

//        eventDetaListView.removeHeaderView(headerWithoutPos);
        eventDetaListView.removeHeaderView(addedHeader);
        mDbHelper.open();
        // alarmList = mDbHelper.getAlarmList();
        localEventDataList = mDbHelper.getEventDetailList(id,type);
        if (localEventDataList == null) {

            eventDetaListView.setAdapter(null);
        } else if ( localEventDataList.isEmpty()) {

            eventDetaListView.setAdapter(null);
        } else {
            // make adapter of task list
//            if (isPosition){
//                eventDetailListAdapter = new EventDetailListAdapter(localEventDataList, getBaseContext(),true,type);
//            } else {
                eventDetailListAdapter = new EventDetailListAdapter(localEventDataList, getBaseContext(),isPosition,type);
//            }
            // set tasklist adapter to listview

            if (isPosition){
                eventDetaListView.addHeaderView(header, null, false);
                addedHeader = header;
            }else {
                eventDetaListView.addHeaderView(headerWithoutPos, null, false);
                addedHeader = headerWithoutPos;
            }



            eventDetaListView.setAdapter(eventDetailListAdapter);
        }

        mDbHelper.close();

    }
    public void updateDateValues(ArrayList beanList, String m_chosen){

        ArrayList dataList = new ArrayList();

        try {

            List<String[]> data  = toStringArray(beanList);

//            String csv = "data.csv";

//          CSVWriter writer = new CSVWriter(new FileWriter(csv));
            CSVWriter writer = new CSVWriter(new FileWriter( m_chosen + "/data.csv"));

            writer.writeAll(data);

            writer.close();
            showToast(getResources().getString(R.string.file_saved));
//            private static String FILE = Environment.getExternalStorageDirectory()
//
//                    + "/firstexcell.xls"
//         String FILE = m_chosen
//
//                    + "/data.csv";
//            File file = new File(FILE);
//            if (!(file.exists())) {
//                try {
//                    file.createNewFile();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    Log.e("FileCreated", "filsesss");
//                }
//            }
//
//            // File file = new File(context.getExternalFilesDir(null), fileName);
//            FileOutputStream os = null;
//
//            try {
//                os = new FileOutputStream(file);
//                wb.write(os);
//                Log.w("FileUtils", "Writing file" + file);
//                success = true;
//            } catch (Exception e) {
//                Log.w("FileUtils", "Error writing " + file, e);
//            } finally {
//                try {
//                    if (null != os)
//                        os.close();
//                } catch (Exception ex) {
//                }
//            }
//            return success;

        }
        catch (Exception e) {
            e.printStackTrace();

        }

    }
    private  List<String[]> toStringArray(List<EventDetailDTO> data) {
        List<String[]> records = new ArrayList<String[]>();

        //add header record
//        records.add(new String[]{"ID","EVENT NAME","DATE","TIME", "LATITUDE","LONGITUDE"});
        records.add(new String[]{"EVENT NAME","TYPE","EVENT_VALUE","DATE","TIME", "LATITUDE","LONGITUDE"});
        Iterator<EventDetailDTO> it = data.iterator();
        while(it.hasNext()){
            EventDetailDTO eventDetailDTO = it.next();
            String eventValue = "";
            if (type.equalsIgnoreCase(getResources().getString(R.string.situation))){
                eventValue = eventDetailDTO.getSituation();

            } else if (type.equalsIgnoreCase(getResources().getString(R.string.numerical))){
                eventValue = eventDetailDTO.getNumeric();
            } else if (type.equalsIgnoreCase(getResources().getString(R.string.text))){
                eventValue = eventDetailDTO.getText();
            }
            if(isPosition){
                records.add(new String[]{ eventDetailDTO.getEventName(),type,eventValue,eventDetailDTO.getDate(),eventDetailDTO.getTime(),eventDetailDTO.getLatitude(),eventDetailDTO.getLongitude()});
            } else {
                records.add(new String[]{ eventDetailDTO.getEventName(),type,eventValue,eventDetailDTO.getDate(),eventDetailDTO.getTime()});
            }

//          records.add(new String[]{String.valueOf(eventDetailDTO.getId()), eventDetailDTO.getEventName(),eventDetailDTO.getDate(),eventDetailDTO.getTime(),eventDetailDTO.getLatitude(),eventDetailDTO.getLongitude()});

        }
        return records;
    }
    private void showToast(String message) {
        Toast.makeText(ReportActivity.this, message, Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setEventListView();
    }
}